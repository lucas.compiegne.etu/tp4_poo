public class HotelMain {
    public static void main(String[] args) {
        Hotel hotel = new Hotel("California", Status.PREMIUM, 100);

        System.out.println("Number of room : "+hotel.numberOfRooms());

        System.out.println("Room rented : "+hotel.rentRoom(Integer.parseInt(args[0])));
        //System.out.println("Room Rented : "hotel.getRoom(Integer.parseInt(args[0])));

        System.out.println("Number of free Rooms : "+hotel.numberOfFreeRooms());

        hotel.leaveRoom(Integer.parseInt(args[0]));

        System.out.println("Number of free Rooms (after leaving the room) : "+hotel.numberOfFreeRooms());


    }
   } 