
/**
 * An Hotel has a name and some rooms continuously numbered from 1
 *
 */
public class Hotel {

   private final String name;
   private final Status status;
   private Room[] rooms;
   
   /** build an Hotel with given name, status and number of rooms
    * @param name this hotel name
    * @param status this hotel status
    * @param numberOfRooms number of rooms of this hotel
    */
   public Hotel(String name, Status status, int numberOfRooms) {
      this.name = name;
      this.status = status;
      this.rooms = new Room [numberOfRooms+1];
      for(int i = 1; i < numberOfRooms+1; i++){ 
          this.rooms[i] = new Room(i);
      }
   }

    /** return this hotel name
    * @return this hotel name
    */
   public String getName() {
      return this.name;
   }
   /** this hotel status
    * @return this hotel status
    */
   public Status getStatus() {
      return this.status;
   }

   /**  return the number of rooms for this hotel
    * @return the number of rooms for this hotel
    */
   public int numberOfRooms() {
       return this.rooms.length-1;
   }
   
   /** provide the room corresponding to given number, first room has number 1, 
    * number must not be greater than <code>this.numberOfRooms()</code>
    * @param number number of the room, from 1 to this.numberOfRooms()
    * @return the room with given number
    */
   public Room getRoom(int number) {
       return this.rooms[number];
   }

   /** allow to rent the room corresponding to given number if it isn't already rented
    * number must be between 1 and <code>this.numberOfRooms()</code>
    * @param number
    * @return the rented room's number 
    */
   public Room rentRoom(int number) {
       if(number <= 0 || number > this.rooms.length || this.rooms[number].isRent() == true){
            return null; 
        } 
       else {
           this.rooms[number].rent();
           return this.rooms[number];
       }
   }
   
   /** Free a rented room
    * @param number
    */
   public void leaveRoom(int number) {
       if(this.rooms[number].isRent() == true) this.rooms[number].free();
   }
   
   /** return the number of free room in the Hotel
    * @return the number of free room in the Hotel
    */
   public int numberOfFreeRooms() {
       int count = 0;
       for(int i = 1; i < this.rooms.length; i++){
           if(this.rooms[i].isRent() == false) count++;
       }
       return count;
   }   
   
   /** Return the first free room of the Hotel
    * @return the first free room
    */
   public int firstFreeNumber() {
       int count = 0;
        for(int i = 1; i < this.rooms.length; i++){
            if(this.rooms[i].isRent() == false) {
                int a = i;
                return a;
            }
            else count++;
        
        }
        return 0;
   }   
}
